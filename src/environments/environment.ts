// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCJB5AqvRwo2zhnPCAWXUolOYIkGlxHV18",
    authDomain: "projectforpractice-14d00.firebaseapp.com",
    databaseURL: "https://projectforpractice-14d00.firebaseio.com",
    projectId: "projectforpractice-14d00",
    storageBucket: "projectforpractice-14d00.appspot.com",
    messagingSenderId: "354218184398",
    appId: "1:354218184398:web:85ac4d600ae4385426ca9f"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
