import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';

@Component({
  selector: 'app-tempform',
  templateUrl: './tempform.component.html',
  styleUrls: ['./tempform.component.css']
})
export class TempformComponent implements OnInit {

  cities: object[] = [{id:"1",name:"London"},{id:"2",name:"Jerusalem"},{id:"3",name:"Paris"},{id:4, name:"Non-exsisten"}];
  temperature:number;
  city:string;

  onSubmit(){
    this.router.navigate(['/temperatures',this.city]);

  }

  constructor(private router:Router) { }

  ngOnInit() {
  }

}
