import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-editauthor',
  templateUrl: './editauthor.component.html',
  styleUrls: ['./editauthor.component.css']
})
export class EditauthorComponent implements OnInit {

  constructor(private router:Router, private route:ActivatedRoute) { }

  authorsEdit:string;
  authorId:number;

  onSubmitEditAuthor(){
    this.router.navigate(['/authors',this.authorId,this.authorsEdit]);
  }

  ngOnInit() {
    this.authorId = this.route.snapshot.params.author_id;
    this.authorsEdit = this.route.snapshot.params.authorname;
  }

}
