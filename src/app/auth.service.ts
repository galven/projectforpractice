import { Router } from '@angular/router';
import { Users } from './interface/users';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<Users | null>;

  SignUp(email:string, password:string){
        this.afAuth
            .auth
            .createUserWithEmailAndPassword(email,password)
            .then(res =>
              {
              console.log('Succesful sign up',res);
              this.router.navigate(['/posts']);
              }
            );
  }

  getUser(){
    return this.user
  }

  Logout(){
        this.afAuth.auth.signOut().then(res =>console.log('Succesful logout',res))
      }

  login(email:string, password:string){
            this.afAuth
                .auth.signInWithEmailAndPassword(email,password)
                .then(
                  
                  res => 
                  {   console.log('Succesful Login',res);
                      this.router.navigate(['/posts']);
                  }
                )
          }

  constructor(public afAuth:AngularFireAuth, private router:Router) { 
    this.user = this.afAuth.authState;
  }
}
