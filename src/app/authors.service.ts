import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {

  tyepsOfauthors: any =  [{id:1,author:'Lewis Carrol'},{id:2,author:'Leo Tolstoy'},{id:3,author:'Thomas Mann'},{id:4,author:'Yuval Noah Harari'},{id:5,author:'Askol Nevo'}];
  arrLen: number = this.tyepsOfauthors.length;

  getAuthors(): any {
    const authorsObservable = new Observable(observer => {
      setInterval(() => observer.next(this.tyepsOfauthors), 4000)})
      return authorsObservable;
}

addAuthor(authorName:string){
        this.tyepsOfauthors.push({id:this.tyepsOfauthors.length + 1, author: authorName}); 
}

  constructor() { }
}
