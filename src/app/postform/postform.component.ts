import { AuthService } from './../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-postform',
  templateUrl: './postform.component.html',
  styleUrls: ['./postform.component.css']
})
export class PostformComponent implements OnInit {

  title:string;
  body:string;
  username:string;
  id:string;
  userId:string;
  btnText:string = "Add new post";
  isEdit:boolean = false;

  onSubmitAddPost(){
    if(this.isEdit){
      this.postService.updatePost(this.userId,this.id,this.body,this.title,this.username);
    } else {
     this.postService.addPost(this.userId,this.body,this.title,this.username)
    }
    this.router.navigate(['/firebaseposts']);
  }

  constructor(private postService:PostsService, private router:Router,  
    private route: ActivatedRoute, private authService:AuthService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authService.getUser().subscribe(
    user => {
    this.userId = user.uid;    
    if(this.id) {
    this.isEdit = true;
    this.btnText = 'Update post'   
    this.postService.getPost(this.userId,this.id).subscribe(
      post => {
        this.body = post.body;
        this.title = post.title; 
        this.username = post.username;
      }
         )}
      })
  }

}
