import { Observable } from 'rxjs';
import { TempService } from './../temp.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Weather } from './../interface/weather';


@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {

  likes = 0;
  temperature:number;
  city:string;
  image;
  $tempData:Observable<Weather>
  errorMessage:string; 
  hasError:boolean = false; 

  addLikes(){
    this.likes++
  }

  constructor(private route:ActivatedRoute,private tempService:TempService,) { }

  ngOnInit() {
  //  this.temperature = this.route.snapshot.params.temp;
    this.city = this.route.snapshot.params.city;
    this.$tempData = this.tempService.searchWeatherData(this.city);
        this.$tempData.subscribe(
          data => {
            this.temperature = data.temperature;
            this.image = data.image;
          },
          error =>{
                   console.log(error.message);
                    this.hasError = true; 
                   this.errorMessage = error.message; 
                   } 
        )}
}
