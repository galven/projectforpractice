import { Injectable } from '@angular/core';
import { Observable, throwError} from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { tap, catchError, map, flatMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

 // books: any =  [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]
  
  // getBooks(): any {
  //       const booksObservable = new Observable(observer => {
  //              setTimeout(() => {
  //                  observer.next(this.books);
  //              }, 5000);
  //       });
    
  //       return booksObservable;
  //   }

    getBooks(): Observable<any[]> {
       return this.db.collection('books').valueChanges({idField: 'id'});
    }

    getBook(id:string):Observable<any>{
          return this.db.doc(`books/${id}`).get();
        }

    updateBook(id:string,title:string,author:string){
              this.db.doc(`books/${id}`).update(
                {
                  title:title,
                  author:author
                }
              )
       }

    addBook(title:string,author:string){
      const book = {title:title,author:author}
      this.db.collection('books').add(book);
    }

    deleteBook(id:string){
          this.db.doc(`books/${id}`).delete();
        }

    /*
  getBooks(){
    setInterval(() => {return this.books}, 1000);
   }
  */
    
  constructor(private db:AngularFirestore) { }
}
