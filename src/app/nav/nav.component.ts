import { AuthService } from './../auth.service';
import { Routes, Router } from '@angular/router';
import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Location } from "@angular/common";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {

  title:string = "Books";

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, location:Location, router:Router, public authService:AuthService) {

    router.events.subscribe(val => {
      if (location.path() == "/books" || location.path() == "/bookform" ) {
       this.title = 'Books';
     } else if (location.path() == "['/temperatures','London']") {
       this.title = "Temperatures";
   } else if (location.path() == "['/authors',0,'GalVentura']"){
     this.title = "Authors";
   } else if (location.path() == "/tempform"){
     this.title = "Temperatures Form"
   } else if (location.path() == "/posts"){
    this.title = "Posts"
     }else if  (location.path() == "/login")
     this.title = "Log in"
    });   




  }

}
