import { AuthService } from './../auth.service';
import { PostsService } from './../posts.service';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-firebaseposts',
  templateUrl: './firebaseposts.component.html',
  styleUrls: ['./firebaseposts.component.css']
})
export class FirebasepostsComponent implements OnInit {

  panelOpenState = false;
  $FBPosts: Observable<any[]>
  userId:string;

  constructor(private postsService:PostsService, private authService:AuthService) { }

  deletePost(id:string){
    this.postsService.deletePost(this.userId,id)
  }
  
  ngOnInit() {
    console.log("NgOnInit started")  
        this.authService.getUser().subscribe(
          user => {
            this.userId = user.uid;
            this.$FBPosts = this.postsService.getAllPosts(this.userId); 
          }
        )
  //  this.$FBPosts = this.postsService.getAllPosts();
  }

}
