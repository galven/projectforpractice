import { DocformComponent } from './docform/docform.component';
import { SignupComponent } from './signup/signup.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { BooksComponent } from './books/books.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {RouterModule, Routes } from '@angular/router';
import {MatCardModule} from '@angular/material/card';
import {AuthorsComponent } from './authors/authors.component';
import {TempformComponent } from './tempform/tempform.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import {FormsModule}   from '@angular/forms';
import {EditauthorComponent } from './editauthor/editauthor.component';
import {HttpClientModule } from '@angular/common/http';
import {PostsComponent } from './posts/posts.component';
import {AngularFireModule } from '@angular/fire';
import {environment } from '../environments/environment';
import {AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { BookformComponent } from './bookform/bookform.component';
import { FirebasepostsComponent } from './firebaseposts/firebaseposts.component';
import { PostformComponent } from './postform/postform.component';
import { AngularFireAuth } from '@angular/fire/auth';
import {AngularFireAuthModule } from '@angular/fire/auth';
import { LoginComponent } from './login/login.component';
import { ClassifyComponent } from './classify/classify.component';




const appRoutes: Routes = [
  { path: 'books', component: BooksComponent},
  { path: 'bookform', component: BookformComponent},
  { path: 'bookform/:id', component: BookformComponent},
  { path: 'temperatures/:city', component: TemperaturesComponent},
  { path: 'authors',component:AuthorsComponent},
  { path: 'authors/:authorId/:authorEdit',component:AuthorsComponent},
  { path: 'editauthor/:author_id/:authorname',component:EditauthorComponent},
  { path: 'tempform',component:TempformComponent},
  { path: 'posts',component:PostsComponent},
  { path: 'postform',component:PostformComponent},
  { path: 'postform/:id',component:PostformComponent},
  { path: 'firebaseposts',component:FirebasepostsComponent},
  { path: 'signup',component:SignupComponent},
  { path: 'login',component:LoginComponent},
  { path: 'classify',component:DocformComponent},
  { path: 'classified',component:ClassifyComponent},

  { path: '', redirectTo: '/books', pathMatch: 'full'},
]

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    NavComponent,
    BooksComponent,
    TemperaturesComponent,
    AuthorsComponent,
    TempformComponent,
    EditauthorComponent,
    ClassifyComponent,
    DocformComponent,
    PostsComponent,
    BookformComponent,
    FirebasepostsComponent,
    
    PostformComponent,
    SignupComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    RouterModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'projectForPractice'),
    MatInputModule,
    HttpClientModule,
    FormsModule,
    AngularFireAuthModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true} // <-- debugging purposes only
    )   
  ],
  providers: [AngularFirestore, AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
