import { AuthorsService } from './../authors.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

  author:string;
  authorFromURL:string;
  idFromURL:number;
  $authors:Observable<any>
  addAuthor:string;
 // tyepsOfauthors: object[] =  [{id:1,author:'Lewis Carrol'},{id:2,author:'Leo Tolstoy'},{id:3,author:'Thomas Mann'},{id:4,author:'Yuval Noah Harari'},{id:5,author:'Askol Nevo'}];

  constructor(private route:ActivatedRoute, private router:Router, private authorService:AuthorsService) { }

  ngOnInit() {

    this.authorFromURL = this.route.snapshot.params.authorEdit;
    this.idFromURL = this.route.snapshot.params.authorId;      
    this.$authors = this.authorService.getAuthors();
  }

  onSubmit(){
    this.authorService.addAuthor(this.addAuthor);
  }

}
