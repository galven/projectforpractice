import { AuthService } from './auth.service';
import { Post } from './interface/post';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './interface/user';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private urlPosts = "https://jsonplaceholder.typicode.com/posts";
  private urlUsers = "https://jsonplaceholder.typicode.com/users";

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  postCollection:AngularFirestoreCollection

  getPosts(){
    return this.http.get<Post[]>(this.urlPosts);
  }

  getUsers(){
    return this.http.get<User[]>(this.urlUsers);
  }

  addPost(userId:string,body:string,title:string,username:string){
    const post = {body:body,title:title,username:username}
  //  this.db.collection('posts').add(post);
   this.userCollection.doc(userId).collection('posts').add(post);
  }

  getAllPosts(userId):Observable<any[]> {
  //  return this.db.collection('posts').valueChanges({idField: 'id'})
    this.postCollection = this.db.collection(`users/${userId}/posts`);
    console.log('posts collection created');
    return this.postCollection.snapshotChanges().pipe(
      map(collection => collection.map(document => {
                const data = document.payload.doc.data();
                data.id = document.payload.doc.id;
                return data;
      }))
    );  
 }

 updatePost(userId:string,id:string,body:string,title:string,username:string){
  this.db.doc(`users/${userId}/posts/${id}`).update(
    {
      body:body,
      title:title,
      username:username
    }
  )
}

 getPost(userId,id:string):Observable<any>{
  return this.db.doc(`users/${userId}/posts/${id}`).get();
}

deletePost(userId:string,id:string){
  this.db.doc(`users/${userId}/posts/${id}`).delete();
}

  constructor(private http: HttpClient, private db:AngularFirestore, private authService:AuthService) { }
}
