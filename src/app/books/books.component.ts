import { BooksService } from './../books.service';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  panelOpenState = false;
 // books: object[] =  [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]
    $books:Observable<any[]>

    deleteBook(id){
          this.bookService.deleteBook(id)
        }

  constructor(private bookService:BooksService) { }

  ngOnInit() {
    this.$books = this.bookService.getBooks();
  }

}
