import { BooksService } from './../books.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { BooksComponent } from '../books/books.component';

@Component({
  selector: 'app-bookform',
  templateUrl: './bookform.component.html',
  styleUrls: ['./bookform.component.css']
})
export class BookformComponent implements OnInit {

  title:string;
  author:string;
  id:string;
  isEdit:boolean = false;
  buttonText:string = 'Add new book';

  onSubmitAddBook(){
    if(this.isEdit){
            this.bookService.updateBook(this.id,this.title,this.author);
          } else {
           this.bookService.addBook(this.title,this.author)
          }
          this.router.navigate(['/books']);
  }

  constructor(private bookService:BooksService, private router:Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
        if(this.id) {
        this.isEdit = true;
        this.buttonText = 'Update book'   
        this.bookService.getBook(this.id).subscribe(
          book => {
            this.author = book.author;
            this.title = book.title; 
          }
        )}
  }

}
